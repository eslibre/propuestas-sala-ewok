---
layout: 2021/post
category: talks
author: Rosalía Rozalén Heras
title: Traducción colaborativa de Medieval Hackers de Kathleen E. Kennedy
---

## {{ page.author }} - {{ page.title }}

Esta charla presenta la traducción colaborativa del libro Medieval Hackers. El libro de la autora Kathleen E. Kennedy está publicado originalmente en inglés con una licencia creative commons. Este tipo de licencia ha permitido su traducción colaborativa al castellano.

## Formato de la propuesta

Indicar uno de estos:
-   [x]  Charla corta (10 minutos)
-   [ ]  Charla (25 minutos)

## Descripción

Esta charla presenta la traducción colaborativa del libro Medieval Hackers. El libro es de la autora Kathleen E. Kennedy, está publicado originalmente en inglés con una licencia creative commons. Este tipo de licencia ha permitido su traducción colaborativa al castellano.

El libro describe como la mentalidad y/o cultura hacker se encuentra presente desde mucho antes de la aparición de las tecnologías en conceptos y filosofías como el procomún. Concretamente, el libro describe procesos de "hackeo" de fenómenos culturales durante al Edad Media.

La traducción se ha hecho de forma colaborativa entre 8 profesores. El resultado final se publicará en internet al final de este curso gracias a la licencia CC de la obra original.

-   Web del proyecto:

## Público objetivo

Profesores que deseen abordar una labor similar de traducción de textos con licencias libres.

## Ponente(s)

Rosalía Rozalén. Consultora de comunicación y social media. Profesora en el Máster de Dirección de Arte de Universidad Complutense de Madrid y Voxel School. Profesora de formación continua de la UCJC.

### Contacto(s)

-   Nombre: Rosalía Rozalén Heras
-   Email: <rrozalen@rosaliarozalen.com>
-   Web personal:
-   Mastodon (u otras redes sociales libres):
-   Twitter: <https://twitter.com/rrozalen>
-   GitLab:
-   Portfolio o GitHub (u otros sitios de código colaborativo):

## Comentarios
