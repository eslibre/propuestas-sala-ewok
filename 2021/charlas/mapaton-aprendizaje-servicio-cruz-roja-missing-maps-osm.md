---
layout: 2021/post
category: talks
author: Marta Serrano
title: Mapatón aprendizaje servicio con Cruz Roja a través de Missing Maps y OpenStreetMaps
---

## {{ page.author }} - {{ page.title }}

El pasado 18 de marzo, los grados en Artes Digitales, Gestión Urbana y Transporte y Logística de la UCJC participaron en un mapatón organizado junto a Cruz Roja. En esta actividad, siguiendo la filosofía del crowdsourcing, alumnado y profesorado de los tres grados ayudaron a mapear áreas de Burundi donde Cruz Roja tiene desplegada una misión de atención a emergencias. Estas áreas son espacios vacíos en los mapas oficiales y digitales. Las herramientas fueron Missing Maps y Open Street Maps.  

## Formato de la propuesta

Indicar uno de estos:
-   [x]  Charla corta (10 minutos)
-   [ ]  Charla (25 minutos)

## Descripción

El pasado 18 de marzo, los grados en Artes Digitales, Gestión Urbana y Transporte y Logística de la UCJC participaron en un mapatón organizado junto a Cruz Roja. En esta actividad, siguiendo la filosofía del crowdsourcing, alumnado y profesorado de los tres grados ayudaron a mapear áreas de Burundi donde Cruz Roja tiene desplegada una misión de atención a emergencias. Estas áreas son espacios vacíos en los mapas oficiales y digitales. Las herramientas fueron Missing Maps y Open Street Maps.

La actividad incluye una formación previa por parte de voluntarios de Cruz Roja para aprender las habilidades digitales necesarias (registro en missing maps, uso de open street maps, identificación de caminos, mapeo, etc). Posteriormente, la actividad se realiza tutorizada por voluntarios en el aula durante dos horas. Más adelante, se da la oportunidad a alumnado y profesorado a seguir colaborando como voluntarios.

-   Web del proyecto:

## Público objetivo

Profesores que quieran replicar estas actividades en sus aulas. Cruz Roja organiza mapatones con regularidad, pues precisan de este trabajo voluntario.

## Ponente(s)

Marta Serrano. Directora del grado en Transporte y Logística de la Universidad Camilo José Cela.

### Contacto(s)

-   Nombre: Marta Serrano
-   Email: <mserrano@ucjc.edu>
-   Web personal: <https://www.ucjc.edu/universidad/profesores/marta-serrano-perez/>
-   Mastodon (u otras redes sociales libres):
-   Twitter:
-   GitLab:
-   Portfolio o GitHub (u otros sitios de código colaborativo):

## Comentarios
