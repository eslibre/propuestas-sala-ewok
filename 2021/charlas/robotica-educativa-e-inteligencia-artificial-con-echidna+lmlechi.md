---
layout: 2021/post
category: talks
author: Jorge Lobo
title: Robótica Educativa e Inteligencia Artificial con Echidna+LMLEchidna
---

## {{ page.author }} - {{ page.title }}

En esta charla se presentará Echidna Black,  una placa de Echidna Educación basada en Arduino que integra diferentes sensores y actuadores y LMLEchidna, un fork de Scratch con bloques específicos para controlar las placas de Echidna Educación e integrar Machine Learning a tus proyectos.


## Formato de la propuesta

Indicar uno de estos:
-   [ ]  Charla corta (10 minutos)
-   [x]  Charla (25 minutos)

## Descripción

En esta charla se realizará un pequeño proyecto de robótica educativa e Inteligencia Artificial en directo usando herramientas libres: Echidna Black y LMLEchidna.

Echidna Black una placa basada en Arduino pensada para ser usada con entornos visuales de programación desarrollada por tres profesores: Xabier Rosas, José Pujol y Jorge Lobo. Al tener los sensores y actuadores integrados, elimina  la necesidad de cablear, minimizando los errores de electrónica, lo que conlleva un incremento en el tiempo dedicado a programación y al conocimiento de los componentes en el aula y facilita la el aprendizaje de la programación de dispositivos físicos a todo tipo de usuarios.

LMLEchidna es una instancia de LearningML, una plataforma diseñada para la enseñanza y aprendizaje de los fundamentos del Machine Learning a través de aplicaciones prácticas y para el mundo escolar. LMLEchidna está preparada para ser usada con las placas de Echidna Educación, por lo que permite construir un modelo de Machine Learning capaz de reconocer imágenes o textos y usarlo en un programa de EchidnaScratch

Con esto podemos unir la Inteligencia Artificial con la Robótica Educativa, pues en EchidnaScratch se pueden combinar bloques de Machine Learning con bloques de control de la placa y, por supuesto, con todos los demás bloques de programación de Scratch.

-   Web del proyecto: <https://echidna.es/>

## Público objetivo

Personas interesadas en la Robótica Educativa y la Inteligencia Artificial, especialmente aquellas interesadas en su aplicación en el aula de los últimos cursos de Primaria y en Secundaria.

## Ponente(s)

Jorge Lobo es Maestro, Técnico Especialista en Electrónica Industrial y el de Técnico Superior en Animación Sociocultural.
Ha participado en acciones formativas, principalmente orientadas al profesorado, mediante diversas ponencias, talleres, cursos y mesas redondas relacionadas con la Robótica, la Programación y la Inteligencia Artificial.
Participa en proyectos de hardware libre para el aprendizaje de la Programación y la Robótica Educativa como Escornabot y Echidna Educación.

### Contacto(s)

-   Nombre: Jorge Lobo
-   Email:
-   Web personal: <http://www.lobotic.es/>
-   Mastodon (u otras redes sociales libres):
-   Twitter: <https://twitter.com/lobo_tic>
-   GitLab:
-   Portfolio o GitHub (u otros sitios de código colaborativo): <https://github.com/lobotic/>

## Comentarios
