---
layout: 2021/post
category: talks
author: David Alonso Urbano
title: Scratch como herramienta para la enseñanza de la programación en la Educación Primaria
---

## {{ page.author }} - {{ page.title }}

En esta charla se presentan los principales resultados de la tesis: "Scratch como herramienta para la enseñanza de la programación en la Educación Primaria, análisis de usabilidad en la escuela pública de la Comunidad de Madrid".

## Formato de la propuesta

Indicar uno de estos:
-   [x]  Charla corta (10 minutos)
-   [ ]  Charla (25 minutos)

## Descripción

En esta charla se presentan los resultados de la tesis: "Scratch como herramienta para la enseñanza de la programación en la Educación Primaria, análisis de usabilidad en la escuela pública de la Comunidad de Madrid".

En esta tesis se plantea la cuestión acerca de si Scratch es la herramienta más adecuada para aprender a programar. Concretamente, se realiza un estudio que analiza la usabilidad de la herramienta para comprobar si, al aprender Scratch, estamos a aprendiendo la herramienta o pensamiento computacional. Es decir, si la usabilidad permite trabajar el objetivo final -aprender a programar- o se queda en la interfaz -aprender a usar Scratch-. Los resultados se sustentan en una investigación en varias aulas de Primaria de la Comunidad de Madrid.


-   Web del proyecto: <https://repositorio.ucjc.edu/handle/20.500.12020/516?show=full>

## Público objetivo

Profesores que utilizan o desean utilizar Scratch

## Ponente(s)

David Alonso Urbano. Director del grado en Diseño de Videojuegos que se imparte en la Escuela Universitaria ESNE.

### Contacto(s)

-   Nombre: David Alonso Urbano
-   Email: <david.alonso@esne.es>
-   Web personal:
-   Mastodon (u otras redes sociales libres):
-   Twitter: <https://twitter.com/DavidAlonsoVju>
-   GitLab:
-   Portfolio o GitHub (u otros sitios de código colaborativo):

## Comentarios
