---
layout: 2021/post
category: talks
author: Rafael Conde Melguizo
title: Editatón en Wikipedia de mujeres tipógrafas
---

## {{ page.author }} - {{ page.title }}

Dentro de la asignatura Tipografía y maquetación de publicaciones del grado en Artes Digitales se llevó a cabo una editatón para crear y mejorar las biografías de mujeres tipógrafas en Wikipedia.

## Formato de la propuesta

Indicar uno de estos:
-   [x]  Charla corta (10 minutos)
-   [ ]  Charla (25 minutos)

## Descripción

Dentro de la asignatura Tipografía y maquetación de publicaciones del grado en Artes Digitales se llevó a cabo una editatón para crear y mejorar las biografías de mujeres tipógrafas en Wikipedia.

Esta actividad se llevó a cabo con tres objetivos: incluir trabajo práctico de investigación dentro de la parte teórica de la asignatura; liberar el conocimiento generado por los alumnos a través de Wikipedia; trabajar transversalmente el papel de la mujer en el diseño aprovechando el mes de marzo.    

-   Web del proyecto:

## Público objetivo

Profesores que deseen aprender a utilizar las posibilidades de Wikipedia como recurso educativo

## Ponente(s)

Rafael Conde Melguizo. Director del grado en Artes Digitales de la UCJC. Socio de Wikimedia España.

### Contacto(s)

-   Nombre: Rafael Conde Melguizo
-   Email: <rconde@ucjc.edu>
-   Web personal: <https://rafaelcondemelguizo.wixsite.com/laboratorio>
-   Mastodon (u otras redes sociales libres):
-   Twitter: <https://twitter.com/rcondemelguizo>
-   GitLab:
-   Portfolio o GitHub (u otros sitios de código colaborativo):

## Comentarios
