---
layout: 2021/post
section: proposals
category: others
author: NOMBRE
title: TÍTULO-ACTIVIDAD
---

## Resumen

[RESUMEN: Pequeña introducción y motivación de la propuesta.]

## Tipo de actividad

[TIPO DE ACTIVIDAD: Describe de qué tipo de actividad se trata.]

## Descripción

[DESCRIPCIÓN: Descripción algo más extensa sobre la temática y el contenido de la propuesta.]

-   Web del proyecto: [URL]

## Público objetivo

[PÚBLICO OBJETIVO: ¿A quién va dirigida?]

## Ponente(s)

[PONENTE(S): Necesitaríamos algo de información general sobre la persona o personas que van a llevar a cabo la propuesta: intereses personales, experiencia en la materia, si has realizado actividades similares...]

### Contacto(s)

-   Nombre: [NOMBRE]
-   Email: [EMAIL]
-   Web personal: [URL]
-   Mastodon (u otras redes sociales libres): [URL]
-   Twitter: [URL]
-   Gitlab: [URL]
-   Portfolio o GitHub (u otros sitios de código colaborativo): [URL]

## Prerrequisitos para los asistentes

[PRERREQUISITOS PARA LOS ASISTENTES: Descripción de conocimientos mínimos recomendados, así como hardware/software del que deberían disponer las personas asistentes a la actividad.]

## Prerrequisitos para la organización

[PRERREQUISITOS PARA LA ORGANIZACIÓN: Descripción de elementos que sería ideal que la organización pudiera proveer para la correcta realización de la actividad (ejemplo: disposición de máquinas virtuales conun determinado software instalado para las personas que asistan a la actividad). La organización no garantiza que se puedan satisfacer las necesidades de las actividades propuestos.]

## Comentarios

[COMENTARIOS: Cualquier otro comentario relevante para la organización.]

## Preferencias de privacidad

(Si quieres que tu información de contacto sea anónima, mándamos las propuesta mediante los formularios de la web: <https://propuestas-ewok.eslib.re/2021/propuestas/otras/>)

-   [x]  Doy permiso para que mi email de contacto sea publicado con la información de la actividad.
-   [x]  Doy permiso para que mis redes sociales sean publicadas con la información de la actividad.

## Condiciones aceptadas

-   [x]  Acepto seguir el [código de conducta](https://eslib.re/conducta/) de esLibre y solicitar a las personas asistentes su cumplimiento.
-   [x]  Confirmo que al menos una persona de entre las que proponen la actividad estará conectada el día programado para realizarla.
