---
layout: 2021/post
section: proposals
category: others
author: NAME
title: ACTIVITY-TITLE
---

## Abstract

[ABSTRACT: Short introduction and motivation of the proposal.]

## Type of activity

[TYPE OF ACTIVITY: Describe what type of activity it is.]

## Description

[DESCRIPTION: More extensive description of the subject and content of the proposal.]

-   Project website: [URL]

## Target audiences

[TARGET AUDIENCES: To whom?]

## Speaker/s

[SPEAKER/S: Need some general information about the person or persons who will carry out the proposal: personal interests, experience in the subject, if you have carried out similar activities...]

### Contact/s

-   Name: [NAME]
-   Email: [EMAIL]
-   Personal website: [URL]
-   Mastodon (or other free social networks): [URL]
-   Twitter: [URL]
-   Gitlab: [URL]
-   Portfolio or GitHub (or other collaborative code sites): [URL]

## Prerequisites for attendees

[PREREQUISITES FOR ATTENDEES: Description of minimum recommended knowledge, as well as hardware/software that the people attending the activity should have.]

## Prerequisites for the organization

[PREREQUISITES FOR THE ORGANIZATION: Description of elements that the organization could ideally provide for the correct performance of the activity (example: provision of virtual machines with certain software installed for the people who attend the activity). The organization does not guarantee that the needs of the proposed activities can be met.]

## Comments

[COMMENTS: Any other comment relevant to the organization.]

## Privacy preferences

(If you want your contact information to be anonymous, you can send us the proposals using the forms on the web: <https://propuestas-ewok.eslib.re/2021/proposals/others/>)

-   [x]  I give permission for my contact email to be published with the activity information.
-   [x]  I give permission for my social networks to be published with the activity information.

## Accepted conditions

-   [x]  I agree to follow the esLibre [conduct code](https://eslib.re/conduct/) and to ask those attending to comply with it.
-   [x]  I confirm that at least one person will be online on the day scheduled to carry out the activity.
